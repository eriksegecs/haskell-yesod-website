FROM fpco/stack-build as build-app

RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
RUN sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
RUN apt-get update;exit 0
RUN apt-get install -y postgresql postgresql-contrib
CMD ["stack", "exec", "--", "yesod", "devel"]
WORKDIR /src
RUN stack install yesod-bin
RUN stack build aulahaskell
ENV PORT 3000
COPY . /src
