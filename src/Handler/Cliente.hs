{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}
module Handler.Cliente where

import Import
import Text.Lucius
import Text.Julius
import Yesod.Static
import Settings.StaticFiles

formCliente :: Maybe Cliente -> Form Cliente
formCliente mc = renderDivs $ Cliente 
    <$> areq textField "Nome: "  (fmap clienteNome mc) 
    <*> areq textField "Cpf:  "  (fmap clienteCpf  mc)
    <*> areq intField  "Idade: " (fmap clienteIdade mc)
    
getClienteR :: Handler Html
getClienteR = do
    (widget,_) <- generateFormPost (formCliente Nothing)
    msg <- getMessage
    defaultLayout $ do
        usuario <- lookupSession "_ID"
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)

        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/cliente.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("2").setAttribute("class","nav-item active")
                });
            |]


postClienteR :: Handler Html
postClienteR = do
    ((result,_),_) <- runFormPost (formCliente Nothing)
    case result of
        FormSuccess cliente -> do
            runDB $ insert cliente
            setMessage [shamlet|
                <div>
                    CLIENTE INSERIDO COM SUCESSO!
            |]
            redirect ClienteR
        _ -> redirect HomeR
    
getPerfilR :: ClienteId -> Handler Html
getPerfilR cid = do
    usuario <- lookupSession "_ID"
    cliente <- runDB $ get404 cid
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/clienteperfil.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("2").setAttribute("class","nav-item active")
                });
            |]

getListaCliR :: Handler Html
getListaCliR = do
    usuario <- lookupSession "_ID"
    -- clientes = [Entity 1 (Cliente "Teste" "..." 45), Entity 2 (Cliente "Teste2" "..." 18), ...]
    clientes <- runDB $ selectList [] [Asc ClienteNome]
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        --datatables
        addStylesheetRemote "https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"
        addScriptRemote "https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"
        addScriptRemote "https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"
        addStylesheetRemote "https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"
        --end datatables
        addStylesheet (StaticR css_style_css)
        toWidget
                [julius|
                    $(document).ready( function () {
                        $('#table_id').DataTable( {
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    text: 'Cadastrar Cliente',
                                    action: function ( e, dt, node, config ) {
                                        window.location = '/cliente';
                                    }
                                }
                            ]
                        } );
                    } );
                |]
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/clientes.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("2").setAttribute("class","nav-item active")
                });
            |]

-- delete from cliente where id = cid
postApagarCliR :: ClienteId -> Handler Html
postApagarCliR cid = do
    runDB $ delete cid
    redirect ListaCliR


getEditarCliR :: ClienteId -> Handler Html
getEditarCliR cid = do
    usuario <- lookupSession "_ID"
    cliente <- runDB $ get404 cid
    (widget,_) <- generateFormPost (formCliente (Just cliente))
    msg <- getMessage
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/clienteedit.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("2").setAttribute("class","nav-item active")
                });
            |]


postEditarCliR :: ClienteId -> Handler Html
postEditarCliR cid = do
    clienteAntigo <- runDB $ get404 cid
    ((result,_),_) <- runFormPost (formCliente Nothing)
    case result of
        FormSuccess novoCliente -> do
            runDB $ replace cid novoCliente
            redirect ListaCliR
        _ -> redirect HomeR 