{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}
module Handler.Login where

import Import
import Text.Lucius
import Text.Julius
import Yesod.Static
import Settings.StaticFiles

formLogin ::  Form Usuario
formLogin = renderDivs $ Usuario
    <$> areq textField "E-mail: "  Nothing
    <*> areq passwordField "Senha:  "  Nothing
        
getAutR :: Handler Html
getAutR = do
    usuario <- lookupSession "_ID"
    (widget,_) <- generateFormPost formLogin
    msg <- getMessage
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/login.hamlet")
        $(whamletFile "templates/footer.hamlet")



postAutR :: Handler Html
postAutR = do
    ((result,_),_) <- runFormPost formLogin
    case result of
        FormSuccess (Usuario "root@root.com" "root") -> do
            setSession "_ID" "admin"
            redirect AdminR
        FormSuccess (Usuario email senha) -> do
            usuarioExiste <- runDB $ getBy (UniqueEmail2 email)
            case usuarioExiste of
                Nothing -> do
                    setMessage [shamlet|
                        Usuario nao cadastrado
                    |]
                    redirect AutR
                Just (Entity _ usuario) -> do
                    if senha == usuarioSenha usuario then do
                        setSession "_ID" (usuarioEmail usuario)
                        redirect HomeR
                    else do
                        setMessage [shamlet| 
                            Usuario e/ou Senha não conferem
                        |]
                        redirect AutR
        _ -> redirect HomeR
    
postSairR :: Handler Html
postSairR = do
    deleteSession "_ID"
    redirect HomeR


getAdminR :: Handler Html
getAdminR = do
    usuario <- lookupSession "_ID"
    defaultLayout $ do 
        addStylesheet (StaticR css_style_css)
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        [whamlet| 
            Bem vindo, ADMIN
        |]
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/home.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("5").setAttribute("class","nav-item active")
                });
            |]