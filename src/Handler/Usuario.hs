{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}
module Handler.Usuario where

import Import
import Text.Lucius
import Text.Julius
import Yesod.Static
import Settings.StaticFiles

formLogin ::  Form (Usuario, Text)
formLogin = renderDivs $ (,) 
    <$> (Usuario 
        <$> areq textField "E-mail: "  Nothing
        <*> areq passwordField "Senha:  "  Nothing
        )
    <*> areq passwordField  "Confirmação: " Nothing
        
getUsuarioR :: Handler Html
getUsuarioR = do
    usuario <- lookupSession "_ID"
    (widget,_) <- generateFormPost formLogin
    msg <- getMessage
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/usuario.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("4").setAttribute("class","nav-item active")
                });
            |]


postUsuarioR :: Handler Html
postUsuarioR = do
    ((result,_),_) <- runFormPost formLogin
    case result of
        FormSuccess (usuario@(Usuario email senha), conf) -> do
            usuarioExiste <- runDB $ getBy (UniqueEmail2 email)
            case usuarioExiste of
                Just _ -> do
                    setMessage [shamlet|
                            <div>
                                E-MAIL JA CADASTRADO!
                        |]
                    redirect UsuarioR
                Nothing -> do   
                    if senha == conf then do
                        runDB $ insert usuario
                        setMessage [shamlet|
                            <div>
                                USUARIO INSERIDO COM SUCESSO!
                        |]
                        redirect UsuarioR
                    else do 
                        setMessage [shamlet|
                            <div>
                                SENHA E CONFIRMAÇÃO DIFERENTES!
                        |]
                        redirect UsuarioR
        _ -> redirect HomeR
    