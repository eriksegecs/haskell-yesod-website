{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}
module Handler.Imovel where

import Import
import Text.Lucius
import Text.Julius
import Yesod.Static
import Settings.StaticFiles

formImovel :: Maybe Imovel -> Form Imovel
formImovel mc = renderDivs $ Imovel 
    <$> areq textField "Dono: "  (fmap imovelDono mc) 
    <*> areq intField "Área:  "  (fmap imovelArea mc) 
    <*> areq textField  "Endereço: " (fmap imovelEndereco mc) 
    

getImovelR :: Handler Html
getImovelR = do
    usuario <- lookupSession "_ID"
    (widget,_) <- generateFormPost (formImovel  Nothing)
    msg <- getMessage
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/imovel.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("3").setAttribute("class","nav-item active")
                });
            |]

getEditarImovelR :: ImovelId -> Handler Html
getEditarImovelR cid = do
    usuario <- lookupSession "_ID"
    imovel <- runDB $ get404 cid
    (widget,_) <- generateFormPost (formImovel (Just imovel))
    msg <- getMessage
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/imoveledit.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("3").setAttribute("class","nav-item active")
                });
            |]


postEditarImovelR :: ImovelId -> Handler Html
postEditarImovelR cid = do
    imovelAntigo <- runDB $ get404 cid
    ((result,_),_) <- runFormPost (formImovel Nothing)
    case result of
        FormSuccess novoImovel -> do
            runDB $ replace cid novoImovel
            redirect ListaImoR
        _ -> redirect HomeR 


postImovelR :: Handler Html
postImovelR = do
    ((result,_),_) <- runFormPost (formImovel Nothing)
    case result of
        FormSuccess imovel -> do
            runDB $ insert imovel
            setMessage [shamlet|
                <div>
                    imovel INSERIDO COM SUCESSO!
            |]
            redirect ImovelR
        _ -> redirect HomeR
    

getImoR :: ImovelId -> Handler Html
getImoR cid = do
    usuario <- lookupSession "_ID"
    imovel <- runDB $ get404 cid
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/imovelperfil.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
            [julius|
                $(function() {document.getElementById("3").setAttribute("class","nav-item active")
                });
            |]

getListaImoR :: Handler Html
getListaImoR = do
    usuario <- lookupSession "_ID"
    imoveis <- runDB $ selectList [] [Asc ImovelDono]
    defaultLayout $ do
        addScriptRemote "https://code.jquery.com/jquery-3.6.0.js"
        addScriptRemote "http://code.jquery.com/jquery-3.6.0.min.js"
        addScriptRemote "https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.9.2/umd/popper.min.js"
        addScriptRemote "https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/8.5.22/mmenu.js"
        addStylesheetRemote "https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        --datatables
        addStylesheetRemote "https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"
        addScriptRemote "https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"
        addScriptRemote "https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"
        addStylesheetRemote "https://cdn.datatables.net/buttons/1.7.0/css/buttons.dataTables.min.css"
        --end datatables
        addStylesheet (StaticR css_style_css)
        $(whamletFile "templates/header.hamlet")
        $(whamletFile "templates/imoveis.hamlet")
        $(whamletFile "templates/footer.hamlet")
        toWidget
                [julius|
                    $(document).ready( function () {
                        $('#table_id').DataTable( {
                            dom: 'Bfrtip',
                            buttons: [
                                {
                                    text: 'Cadastrar Imóvel',
                                    action: function ( e, dt, node, config ) {
                                        window.location = '/imovel';
                                    }
                                }
                            ]
                        } );
                    } );
                |]
        toWidget
            [julius|
                $(function() {document.getElementById("3").setAttribute("class","nav-item active")
                });
            |]


-- delete from imovel where id = cid
postApagarImoR :: ImovelId -> Handler Html
postApagarImoR cid = do
    runDB $ delete cid
    redirect ListaImoR